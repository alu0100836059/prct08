require "prc8/version"

#lista_doble.rb

# Incluimos Biblioteca.rb para comprobar el funcionamiento de la clase List_Doble
####################################################################################################################################
############ ACORDARSE DE CAMBIAR ESTA REFERENCIA AL EJECUTARLO FUERA DE ESTE PC ###################################################
####################################################################################################################################

Node = Struct.new(:ant, :punt, :sig)

class List_Doble
	attr_accessor :first, :last, :num_items

def initialize()
	# Inicializamos ya el tipo de dato de @first y @last así como de @num_items
	@first = Node.new(nil, nil, nil)
	@last = Node.new(nil, nil, nil)
	@num_items = 0
end

def pop()

	# Si tenemos más de un elemento
	if @first.ant != nil
		aux = @first
		@first = @first.ant
		@first.sig = nil
		@first.ant = @first.ant #aux.ant.ant
		@num_items = @num_items - 1
		return aux.punt
	
	# Si solamente hay un elemento
	elsif @first != nil
			aux = @first
			@first.ant = @first.sig = @first = nil
			
			@num_items = @num_items - 1
			return aux.punt
	else	# Si la lista se encuentra vacía
		return nil
	end
end


def push(valor)
	pr = Node.new(nil,valor,nil)
	
	# Si tenemos al menos un elemento
	if @first.punt != nil
		aux = @first

		while aux.ant != nil
			aux = aux.ant
		end
		aux.ant = pr
		aux.ant.sig = @first
		aux.ant.ant = nil 
	else	# Si la lista se encuentra vacía
		@first = pr
	end
	@num_items = @num_items + 1	
end

	def mostrar()

		aux = @first
		if aux.punt == nil
			puts "Lista vacía"
		else
			while aux.ant != nil
				puts aux.punt.mostrar
				aux = aux.ant
			end
			puts aux.punt.mostrar	# Mostrar el último elemento una vez alcanzado el nil
		end
	end
end

#################################################################################################################################################
###################### SOLAMENTE PARA COMPROBACIÓN INTERNA DE LA CLASE ##########################################################################
#################################################################################################################################################

=begin

# Comprobando funcionamiento

# Declaración de los objetos librería
@lib = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])  
@lib_b = Bibliografia.new(["Scot Chacon"],"Pro Git 2009th Edition","(Pro)","Apress", "2009 edition", "(August 27, 2009)", ["ISBN-13: 978-1430218333"," ISBN-10: 1430218339"])
@lib_c = Bibliografia.new(["David Flanagan","Yukihiro Matsumoto"], "The Ruby Programming Language","O’Reilly"," Media","1 edition", "(February 4, 2008)", ["ISBN-10: 0596516177", "ISBN-13: 978-0596516178"])
@lib_d = Bibliografia.new(["David Chelimsky","Dave Astels","Bryan Helmkamp","Dan North","Zach Dennis","Aslak Hellesoy"],"The RSpec Book: Behaviour Driven Development with RSpec","Cucumber and Friends (The Facets of Ruby)","Pragmatic Bookshelf", "1 edition","(December 25, 2010)", ["ISBN-10: 1934356379","ISBN-13: 978-1934356371"])
@lib_e = Bibliografia.new(["Richard E. Silverman"],"Git Pocket Guide","O’Reilly","Media","1 edition","(August 2, 2013)", ["ISBN-10: 1449325866","ISBN-13: 978-1449325862"])


	# Creación de la lista vacía en principio
	@list_final = List_Doble.new()

	# Comprobando si detecta la lista vacía
	puts "-------------- Primera ejecución con la lista vacía -----------------\n"
	@list_final.mostrar
	puts "\n"
	# Creación de los nodos con los objetos librería asignados
    @nodo_a = Node.new(nil,@lib,nil)
    @nodo_b = Node.new(nil,@lib_b,nil)
    @nodo_c = Node.new(nil,@lib_c,nil)
    
    @nodo_d = Node.new(nil,@lib_d,nil)
    @nodo_e = Node.new(nil,@lib_e,nil)

    # Introducimos los nodos en la lista
    @list_final.push(@nodo_a)
    @list_final.push(@nodo_b)
    @list_final.push(@nodo_c)
    @list_final.push(@nodo_d)
    @list_final.push(@nodo_e)

    # Mostramos el estado actual de la lista
    puts "-------------- Segunda ejecución con 5 nodos siendo el primero el referente a: Programming Ruby y el último: Git Pocket Guide ---------------\n"
    @list_final.mostrar

    # Utilizamos gets() para separar el check push y el check pop, pulsamos tecla para continuar la ejecución
    gets()

    # Retiramos un elemento de la lista
    @list_final.pop

    # Incluimos una separación para diferenciar correctamente ambos estados
    puts "--------------- Iniciando Segundo Recorrido ----------------\n"
    puts "--------------- Retirado un solo elemento ------------------\n"
    # Mostramos de nuevo el estado actual de la lista
    @list_final.mostrar

    # Utilizamos gets() para evitar el cierre repentino al finalizar la ejecución
    gets()


=end
