module Bibliografia

require_relative "bibliografia"

	class Revista < Bibliografia
		attr_accessor :pub

		def initialize(tp, pp, t, a, r, n, f, i, ref)
			@tipo_pub = tp.to_s
			@pp = pp.to_s
			@autor = t
			@titulo = a.to_s
			@revista = r.to_s
			@num_edicion = n.to_s
			@fecha_publicacion = f.to_s
			@issn = i
			@referencia = ref.to_s
		end
	end
end