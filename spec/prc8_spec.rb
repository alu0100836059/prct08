module Bibliografia
require 'prc8/bibliografia'
require 'spec_helper'
#before :each do
@lib = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])  
@lib_b = Bibliografia.new(["Scot Chacon"],"Pro Git 2009th Edition","(Pro)","Apress", "2009 edition", "(August 27, 2009)", ["ISBN-13: 978-1430218333"," ISBN-10: 1430218339"])
@lib_c = Bibliografia.new(["David Flanagan","Yukihiro Matsumoto"], "The Ruby Programming Language","O’Reilly"," Media","1 edition", "(February 4, 2008)", ["ISBN-10: 0596516177", "ISBN-13: 978-0596516178"])
@lib_d = Bibliografia.new(["David Chelimsky","Dave Astels","Bryan Helmkamp","Dan North","Zach Dennis","Aslak Hellesoy"],"The RSpec Book: Behaviour Driven Development with RSpec","Cucumber and Friends (The Facets of Ruby)","Pragmatic Bookshelf", "1 edition","(December 25, 2010)", ["ISBN-10: 1934356379","ISBN-13: 978-1934356371"])
@lib_e = Bibliografia.new(["Richard E. Silverman"],"Git Pocket Guide","O’Reilly","Media","1 edition","(August 2, 2013)", ["ISBN-10: 1449325866","ISBN-13: 978-1449325862"])
@nodo = Node.new(nil,@lib,nil)
@list = List_Doble.new()
@list.push(@nodo)

#end
#@nodo_digi = Node.new(nil,@digital,nil)
#@nodo_libr = Node.new(nil,@libro,nil)
#@nodo_revi = Node.new(nil,@revista,nil)
#@list_definitiva = List_Doble.new()
#@list_definitiva.push(@nodo_libr)
describe "Nodo" do

it "Debe existir un nodo de la lista con sus datos" do
	@nodo_ = Node.new(nil,@lib,nil)
	expect(@nodo_.punt).to equal(@lib)
	expect(@nodo_.sig).to equal(nil)
	expect(@nodo_.ant).to equal(nil)

end
end

describe "Lista" do
@lib = Bibliografia.new(['as','as'],'as','as','as','as','as',['as','as'])
@nodo = Node.new(nil,@lib,nil)
@nodo_c = Node.new(nil,@lib,nil)
@list = List_Doble.new()
@list.push(@nodo)
@list.push(@nodo_c)

it "Se extrae el primer elemento de la lista" do
  @list_ = List_Doble.new()
  @list_.num_items.should be == 0 #dif
  @nodo_a = Node.new(nil,@lib,nil)
  @list_.push(@nodo_a)
  @list_.num_items.should be == 1
  @list_.pop
  @list_.num_items.should be == 0 #dif
end

it "Se puede insertar un elemento" do
  @list_ = List_Doble.new()
  @list_.num_items.should be == 0
  @list_.push(@nodo_c)
  @list_.num_items.should be == 1
end

it "Se pueden insertar varios elementos" do
  @list_ = List_Doble.new()
  @list_.num_items.should be == 0
  @nodo_c = Node.new(nil,@lib,nil)
  @list_.push(@nodo_c)
  @list_.push(@nodo_c)
  @list_.push(@nodo_c)
  @list_.push(@nodo_c)
  @list_.num_items.should be > 1

end

it "Debe existir una lista con su cabeza" do
  @list_ = List_Doble.new()
  @nodo_c = Node.new(nil,@lib,nil)
  @list_.push(@nodo_c)
  @list_.push(@nodo_c)
  expect(@list_.first).not_to equal(nil)
end
end


describe "Comprobacion tipos de referencias bibliograficas" do

it "Libro hereda de.." do
@libro = Libro.new('Libro', 'Scott Chacon', 'Pro Git 2009th Edition', '(Pro) Apress', '2009 edition', '', 'August 27, 2009', ['978-1430218333','1430218339'])
  expect(@libro).to be_kind_of(Bibliografia)
  @libro.autor.should == "Scott Chacon"
end

it "Digital hereda de.." do
  @digital = Articulo_Digital.new('Publicacion Periodica', 'Documento electronico', ['Juan Carlos GonzÃ¡lez'], 'Yamaha estÃ¡ desarrollando un espectacular robot capaz de conducir motos de alta cilindrada', 'http://www.xataka.com/robotica-e-ia/yamaha-esta-desarrollando-un-espectacular-robot-capaz-de-conducir-motos-de-alta-cilindrada', '1', '28 de Octubre de 2015')
    expect(@digital).to be_kind_of(Bibliografia)
  @digital.autor.should == ['Juan Carlos GonzÃ¡lez']
end

it "Revista hereda de.." do
  @revista = Revista.new('Publicacion Periodica', 'Artculo de Periodico', ' Javier PalazÃ³n / Laura Pajuelo ', 'El primer mÃ³vil con Android One', 'El Pais', '8889221', '16 Noviembre de 2015', '2929332', 'EAS-1447241991')
  expect(@revista).to be_kind_of(Bibliografia)
  @revista.autor.should == " Javier PalazÃ³n / Laura Pajuelo "
end

end
end